package hu.centralmediacsoport.coffeemachine.machine.utils;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;

import hu.centralmediacsoport.coffeemachine.Application;

/**
 * Created by david.eperjes on 11/11/16.
 */

public class SoundManager {

    private static final String TAG = "SoundManager";

    private static final String SOUNDS_DIR = "sounds/";
    private static final String COIN_SOUND = SOUNDS_DIR + "coin.wav";
    private static final String MACHINE_SOUND = SOUNDS_DIR + "machine.mp3";

    private static MediaPlayer sMediaPlayer;

    private static MediaPlayer getMediaPlayer() {
        if (sMediaPlayer == null) {
            sMediaPlayer = new MediaPlayer();
        }
        releaseMediaPlayer();
        return sMediaPlayer;
    }

    private static void releaseMediaPlayer() {
        if (sMediaPlayer.isPlaying()) {
            sMediaPlayer.stop();
        }
        sMediaPlayer.reset();
    }

    private static void playMedia(String fileName) {
        MediaPlayer player = getMediaPlayer();
        AssetFileDescriptor afd = getFileDescriptor(fileName);
        if (afd == null) {
            Log.e(TAG, "Sound File not found.");
            return;
        }
        try {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void playCoinSound() {
        playMedia(COIN_SOUND);
    }

    public static void playCoffeeMachineSound() {
        playMedia(MACHINE_SOUND);
    }

    public static void stopCoffeeMachineSound() {
        releaseMediaPlayer();
    }

    private static AssetFileDescriptor getFileDescriptor(String fileName) {
        try {
            return Application.getContext().getAssets().openFd(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
