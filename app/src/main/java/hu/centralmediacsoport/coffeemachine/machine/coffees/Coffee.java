package hu.centralmediacsoport.coffeemachine.machine.coffees;

import java.util.ArrayList;
import java.util.List;

import hu.centralmediacsoport.coffeemachine.machine.ingredients.Ingredient;

/**
 * Created by david.eperjes on 08/11/16.
 */

public abstract class Coffee {

    protected List<Ingredient> ingredients = new ArrayList<>();

    public abstract String getName();
    public abstract int getCost();

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public final String getIngredientsString() {
        // TODO implement
        return "n/a";
    }

}
