package hu.centralmediacsoport.coffeemachine.messages;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class CoffeeMachineConsoleLogMessage {

    private String message;

    public CoffeeMachineConsoleLogMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
