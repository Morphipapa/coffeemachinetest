package hu.centralmediacsoport.coffeemachine.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hu.centralmediacsoport.coffeemachine.R;
import hu.centralmediacsoport.coffeemachine.entities.ConsoleLogMessage;

/**
 * Created by david.eperjes on 09/11/16.
 */

public class ConsoleAdapter extends RecyclerView.Adapter<ConsoleAdapter.MessageViewHolder> {

    private Context mContext;
    private List<ConsoleLogMessage> mMessages = new ArrayList<>();

    public ConsoleAdapter(Context context) {
        this.mContext = context;
    }

    public void addMessage(ConsoleLogMessage message) {
        int position = getItemCount();
        if (mMessages.add(message)) {
            notifyItemInserted(position);
        }
    }

    public void addMessages(List<ConsoleLogMessage> messages) {
        int position = getItemCount();
        if (mMessages.addAll(messages)) {
            notifyItemRangeInserted(position, messages.size());
        }
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {

        private TextView time;
        private TextView message;

        public MessageViewHolder(View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.log_time);
            message = (TextView) itemView.findViewById(R.id.log_message);
        }
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_console_message, parent, false);
        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {

        ConsoleLogMessage message = getMessage(position);

        holder.time.setText(message.getDateString());
        holder.message.setText(message.getMessage());

    }

    private ConsoleLogMessage getMessage(int position) {
        return mMessages.get(position);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }
}
