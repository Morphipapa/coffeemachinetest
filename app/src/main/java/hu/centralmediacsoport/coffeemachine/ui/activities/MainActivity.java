package hu.centralmediacsoport.coffeemachine.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import hu.centralmediacsoport.coffeemachine.adapters.ConsoleAdapter;
import hu.centralmediacsoport.coffeemachine.entities.ConsoleLogMessage;
import hu.centralmediacsoport.coffeemachine.machine.CoffeeMachine;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Coffee;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Espresso;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Milk;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Sugar;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineBalanceMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineConsoleLogMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineStatusMessage;
import hu.centralmediacsoport.coffeemachine.R;

public class MainActivity extends AppCompatActivity {

    private CoffeeMachine mMachine = new CoffeeMachine();
    private TextView mStatusDisplay, mBalanceDisplay;
    private ConsoleAdapter mConsoleAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusDisplay = (TextView) findViewById(R.id.display);
        mBalanceDisplay = (TextView) findViewById(R.id.display_balance);

        setupConsole();
        setupButtons();

        EventBus.getDefault().register(this);
    }

    private void setupConsole() {

        mConsoleAdapter = new ConsoleAdapter(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.console);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mConsoleAdapter);

    }

    private void setupButtons() {
        findViewById(R.id.add_coins).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCoins(300);
            }
        });

        findViewById(R.id.money_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moneyBack();
            }
        });

        findViewById(R.id.select_espresso).setOnClickListener(new OnCoffeeCreate(new Espresso()));

        findViewById(R.id.create_coffee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCoffee();
            }
        });

        findViewById(R.id.add_extra).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showExtras();
            }
        });

    }

    private void addCoins(int value) {
        mMachine.pay(value);
    }

    private void moneyBack() {
        mMachine.payBack();
    }

    private void setCoffee(Coffee coffee) {
        mMachine.setCoffee(coffee);
    }

    private void createCoffee() {
        mMachine.getCoffee();
    }

    private void showExtras() {
        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle(R.string.select_extra_title)
                .setItems(R.array.extras, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {

                            case 0: // Milk
                                mMachine.addExtra(new Milk());
                                break;

                            case 1: // Sugar
                                mMachine.addExtra(new Sugar());
                                break;

                            case 2: // Whip
                                // TODO: implement
                                break;

                        }
                        dialogInterface.dismiss();
                    }
                }).create();
        ad.show();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStatusMessage(CoffeeMachineStatusMessage message) {
        mStatusDisplay.setText(message.getMessage());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onBalanceMessage(CoffeeMachineBalanceMessage message) {
        mBalanceDisplay.setText(message.getMessage());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onConsoleMessage(CoffeeMachineConsoleLogMessage message) {
        printConsole(message.getMessage());
    }

    private void printConsole(String message) {
        mConsoleAdapter.addMessage(new ConsoleLogMessage(message));
        mRecyclerView.scrollToPosition(mConsoleAdapter.getItemCount() - 1);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    class OnCoffeeCreate implements View.OnClickListener {

        private Coffee coffee;

        public OnCoffeeCreate(Coffee coffee) {
            this.coffee = coffee;
        }

        @Override
        public void onClick(View view) {
            setCoffee(coffee);
        }
    }
}
